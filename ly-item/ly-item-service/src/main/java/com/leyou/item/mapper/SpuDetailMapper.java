package com.leyou.item.mapper;

import com.leyou.item.pojo.SpuDetail;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author HongBo Wang
 * @data 2018/6/28 20:11
 */
public interface SpuDetailMapper extends Mapper<SpuDetail> {
}
