package com.leyou.item.service;

import com.leyou.item.mapper.SpecGroupMapper;
import com.leyou.item.mapper.SpecParamMapper;
import com.leyou.item.pojo.SpecGroup;
import com.leyou.item.pojo.SpecParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author HongBo Wang
 * @data 2018/6/27 10:50
 */
@Service
public class SpecificationService {
    @Autowired
    private SpecGroupMapper specGroupMapper;

    public List<SpecGroup> querySpecGroupsByCid(Long cid){
        //传一个specGroup对象,里面包含cid
        SpecGroup specGroup = new SpecGroup();
        specGroup.setCid(cid);
        //返回集合
        return this.specGroupMapper.select(specGroup);
    }

    @Autowired
    private SpecParamMapper specParamMapper;

    public List<SpecParam> querySpecParamsByGid(Long gid,Long cid,Boolean searing,Boolean generic) {
        SpecParam specParam = new SpecParam();
        specParam.setGroupId(gid);
        specParam.setCid(cid);
        specParam.setSearching(searing);
        specParam.setGeneric(generic);
        return this.specParamMapper.select(specParam);
    }

    public List<SpecGroup> querySpecGroupAndParam(Long cid) {
        List<SpecGroup> groups = this.querySpecGroupsByCid(cid);
        for (SpecGroup group : groups) {
            List<SpecParam> params = this.querySpecParamsByGid(
                    group.getId(), null, null, null);
            group.setParams(params);
        }
        return groups;
    }
}
