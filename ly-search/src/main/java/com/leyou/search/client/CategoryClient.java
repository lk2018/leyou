package com.leyou.search.client;

import com.leyou.item.api.CategoryApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author HongBo Wang
 * @data 2018/7/1 13:31
 */
@FeignClient(value = "item-service")
public interface CategoryClient extends CategoryApi{
}
